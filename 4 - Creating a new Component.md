# Creating a new Component

To create a new componet the approach is relatively similar, we start off with the CLI tool;
```python
$ orbit_cli --init component

 ██████╗ ██████╗ ██████╗ ██╗████████╗    ██████╗██╗     ██╗
██╔═══██╗██╔══██╗██╔══██╗██║╚══██╔══╝   ██╔════╝██║     ██║
██║   ██║██████╔╝██████╔╝██║   ██║█████╗██║     ██║     ██║
██║   ██║██╔══██╗██╔══██╗██║   ██║╚════╝██║     ██║     ██║
╚██████╔╝██║  ██║██████╔╝██║   ██║      ╚██████╗███████╗██║
 ╚═════╝ ╚═╝  ╚═╝╚═════╝ ╚═╝   ╚═╝       ╚═════╝╚══════╝╚═╝
    Mad Penguin Consulting Ltd (c) 2023 (MIT License)

[OrbitCLI] Ensuring pyenv is set up ...
[OrbitCLI] Ensuring nvm is set up ...
[OrbitCLI] Ensuring npm is set up ...
[OrbitCLI] Ensuring poetry is set up ...
[OrbitCLI] Collecting project information...
[OrbitCLI] Enter your project name (mycomp): 
[OrbitCLI] Enter your project description (A Demo Component): 
[OrbitCLI] Enter your name (project author) (John Doe): 
[OrbitCLI] Enter your email address (bugs email) (support@madpenguin.uk): 
[OrbitCLI] Enter your project home page (https://linux.uk): 
[OrbitCLI] Create project mycomp? [y/n]: y
[OrbitCLI] Creating virtual env 
...
[OrbitCLI] Creating folders ...
[OrbitCLI] Creating folder: client/src/stores
[OrbitCLI] Creating folder: client/src/widgets
[OrbitCLI] Creating folder: client/src/views
[OrbitCLI] Creating folder: server/scripts
[OrbitCLI] Creating folder: scripts
[OrbitCLI] Creating folder: server/orbit_component_mycomp//schema
[OrbitCLI] Installing README.md in .
[OrbitCLI] Installing Makefile in .
[OrbitCLI] Installing .gitignore in .
[OrbitCLI] Installing .version in .
[OrbitCLI] Installing README.md in server
[OrbitCLI] Installing pyproject.toml in server
[OrbitCLI] Installing Makefile in server
[OrbitCLI] Installing __init__.py in server/orbit_component_mycomp/
[OrbitCLI] Installing plugin.py in server/orbit_component_mycomp/
[OrbitCLI] Installing MyTable.py in server/orbit_component_mycomp//schema
[OrbitCLI] Installing roll_version.py in scripts
[OrbitCLI] Installing Makefile in client
[OrbitCLI] Installing package.json in client
[OrbitCLI] Installing vite.config.js in client
[OrbitCLI] Installing index.js in client
[OrbitCLI] Installing menu.js in client
[OrbitCLI] Installing main.vue in client/src/mycomp.vue
[OrbitCLI] Installing mainComponent.vue in client/src/mycompComponent.vue
[OrbitCLI] Installing mytableStore.js in client/src/stores
[OrbitCLI] Installing JS components >>>>
...
[OrbitCLI] <<<<
[OrbitCLI] Installing PY components >>>>
...
[OrbitCLI] <<<<
[OrbitCLI] Complete.
```
In order to develop our component, we need to link it into a test application. You can link it in as a HMR responsive module, but for now I'll link it as an external NPM which needs a 'build' between changes. (less intuitive to use, easier to demo)

```python
# Link the backend of the component into the application
~/my-demo-project/server$ pyenv activate my-demo-project
~/my-demo-project/server$ pip install -e ../../orbit-component-mycomp/server/
# Link the front-end of the component into the application
~/my-demo-project/client$ npx link ../../orbit-component-mycomp/client/
# Build the front-end components
~/orbit-component-mycomp/client$ npm run build
# (note the paths from which these commands are executed)
~$ pyenv deactivate
~$ orbit_cli --update my-demo-project
```
At this point the changes on the back-end should be automatically detected (so long as you're still running the two commands earlier, npm run dev and make run_dev) and the screen should refresh giving;

<img style="background-color: white; height:400px;margin: 1em;border-radius:4px" src="images/demo3.png" />

## What has this done?

When you create an application it automatically inserts the component "orbit-component-shell" which provides the default "look" for your application. You can write your own shell component if you wish just by taking the source for the default shell and changing the bits you need - which will give you any desired look. In this (default) instance, components take the form of buttons which are automatically inserted down the left panel, so you can see our new component "mycomp" now has a button, and if you click on the button the body of the component is displed in the main pane on the right. 

The default component is just set up to demonstrate how a simple component operates and how the ORM mechanism works in terms of mapping a database query onto the screen. In this instance we're relying on an empty table called "mytable" which is expected to hold some "people data". In order to see what it does, we can download some demonstration data with;

```bash
$ orbit_cli --populate my-demo-project

 ██████╗ ██████╗ ██████╗ ██╗████████╗    ██████╗██╗     ██╗
██╔═══██╗██╔══██╗██╔══██╗██║╚══██╔══╝   ██╔════╝██║     ██║
██║   ██║██████╔╝██████╔╝██║   ██║█████╗██║     ██║     ██║
██║   ██║██╔══██╗██╔══██╗██║   ██║╚════╝██║     ██║     ██║
╚██████╔╝██║  ██║██████╔╝██║   ██║      ╚██████╗███████╗██║
 ╚═════╝ ╚═╝  ╚═╝╚═════╝ ╚═╝   ╚═╝       ╚═════╝╚══════╝╚═╝
    Mad Penguin Consulting Ltd (c) 2023 (MIT License)

[OrbitCLI] Populating my-demo-project data
[OrbitCLI] Processing people ...
[OrbitCLI] => 1...
[OrbitCLI] => 2...
[OrbitCLI] => 3...
[OrbitCLI] => 4...
[OrbitCLI] => 5...
[OrbitCLI] => 6...
[OrbitCLI] => 7...
[OrbitCLI] => 8...
[OrbitCLI] => 9...
[OrbitCLI] Complete
```
This should leave a file called "demo_data_people.json" in the root of the project folder. Next we need to install the database shell, which will allow us to import some data into a table.
```bash
pip3 install orbit-database-shell
...
$ orbit_database_shell 

  .oooooo.             .o8        o8o      .           oooooooooo.   oooooooooo.
 d8P'  `Y8b           "888        `"'    .o8           `888'   `Y8b  `888'   `Y8b
888      888 oooo d8b  888oooo.  oooo  .o888oo          888      888  888     888
888      888 `888""8P  d88' `88b `888    888            888      888  888oooo888'
888      888  888      888   888  888    888   8888888  888      888  888    `88b
`88b    d88'  888      888   888  888    888 .          888     d88'  888    .88P
 `Y8bood8P'  d888b     `Y8bod8P' o888o   "888"         o888bood8P'   o888bood8P'

     Orbit Database Command Line Tool (c) Mad Penguin Consulting Ltd 2023
     To get started try help register or help for all available commands
     Version 1.0.9

none> register app /home/dev6/.local/my-demo-project/orbit_database
Registered app => /home/dev6/.local/my-demo-project/orbit_database

none> use app
app> load mytable /home/dev6/my-demo-project/demo_data_people.json
```
Now assuming you left those previous commands running, and your browser page open, the changes to the database will register immmediately with the screen which should now look like this;

<img style="background-color: white; height:400px;margin: 1em;border-radius:4px" src="images/demo4.png" />

I can now edit *~/orbit-component-mycomp/client/src/mycompComponent.vue* and change the display template to say "My Star Wars Component", followed by "npm run build" in the component client folder. This will immediately update the screen. To customise the button;

```python
~/my-demo-project/client/src/components$ cp mycomp.js.template mycomp.js
```

If I now edit mycomp.js and make it look like this;

```javascript
import mycomp from '@/../node_modules/orbit-component-mycomp'
import JediOrder from '@vicons/fa/JediOrder.js'
import { shallowRef } from 'vue'

export function menu (app, router, menu) {
    const IconAPI = shallowRef(JediOrder)
    app.use(mycomp, {
        router: router,
        menu: menu,
        root: 'mycomp',
        buttons: [
            {name: 'mycomp', text: 'Jedi DB'  , component: mycomp , icon: IconAPI , pri: 1, path: '/mycomp', meta: {root: 'mycomp', host: location.host}},
        ]
    })
}
```
The 'fa' icons are not installed by default, so in the application we need to do;
```bash
npm i @vicons/fa                               # npm will remove links!
npx link ../../orbit-component-mycomp/client   # so re-instance our local component link
```
And now we should have a component with a customised template and a customised button;

<img style="background-color: white; height:400px;margin: 1em;border-radius:4px" src="images/demo5.png" />