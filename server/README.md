# Orbit-CLI

#### This tool aims to make things easier when you come to create a new application or a new component. It will generate all the boilerplate files you need (from templates) to get up and running with a minimum of fuss. Whereas it won't do everything for you, it should do 99% of the hard work when it comes to setting up a new project.
