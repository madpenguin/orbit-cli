#!/usr/bin/env python
from orbit_component_base import OrbitMainBase

if __name__ == '__main__':
    OrbitMainBase('{{ project }}').run()
