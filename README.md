# Orbit-CLI

### Note:

As a result of changes to PyPi Terms, this package is no longer available from the standard PyPi server. In order to use
this package with "pip" either add "-i https://pypi.madpenguin.uk" to your command line, or for transparency;
```bash
# Create or add to ~/.config/pip/pip.conf
[global]
extra-index-url = https://pypi.madpenguin.uk
```

#### This tool aims to make things easier when you come to create a new application or a new component. It will generate all the boilerplate files you need (from templates) to get up and running with a minimum of fuss. Whereas it won't do everything for you, it should do 99% of the hard work when it comes to setting up a new project.

```bash
$ orbit_cli --help

 ██████╗ ██████╗ ██████╗ ██╗████████╗    ██████╗██╗     ██╗
██╔═══██╗██╔══██╗██╔══██╗██║╚══██╔══╝   ██╔════╝██║     ██║
██║   ██║██████╔╝██████╔╝██║   ██║█████╗██║     ██║     ██║
██║   ██║██╔══██╗██╔══██╗██║   ██║╚════╝██║     ██║     ██║
╚██████╔╝██║  ██║██████╔╝██║   ██║      ╚██████╗███████╗██║
 ╚═════╝ ╚═╝  ╚═╝╚═════╝ ╚═╝   ╚═╝       ╚═════╝╚══════╝╚═╝
    Mad Penguin Consulting Ltd (c) 2023 (MIT License)

usage: orbit_cli [-h] [--init TYPE] [--update PROJECT] [--build PROJECT] [--list PROJECT] [--add PROJECT] [--rem PROJECT] [--components [COMPONENTS ...]] [--populate PROJECT] [--reset]

options:
  -h, --help            show this help message and exit
  --init TYPE           Specify whether we are working with an 'application' or 'component'
  --update PROJECT      Update main.js based on currently installed components
  --build PROJECT       Build a .DEB intaller for the specified project
  --list PROJECT        List installed components
  --add PROJECT         Add new components
  --rem PROJECT         Remove installed components
  --components [COMPONENTS ...]
                        Components to operate on
  --populate PROJECT    Download sample data into project folder from sample API
  --reset               Remove and reinstall all tools [pyenv, nvm etc]
```
