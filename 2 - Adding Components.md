# Adding Components

Now we have a basic projec, we can add a few components to get going;
```python
$ orbit_cli --add my-demo-project --components orbit-component-buttonlegal orbit-component-zerodocs

 ██████╗ ██████╗ ██████╗ ██╗████████╗    ██████╗██╗     ██╗
██╔═══██╗██╔══██╗██╔══██╗██║╚══██╔══╝   ██╔════╝██║     ██║
██║   ██║██████╔╝██████╔╝██║   ██║█████╗██║     ██║     ██║
██║   ██║██╔══██╗██╔══██╗██║   ██║╚════╝██║     ██║     ██║
╚██████╔╝██║  ██║██████╔╝██║   ██║      ╚██████╗███████╗██║
 ╚═════╝ ╚═╝  ╚═╝╚═════╝ ╚═╝   ╚═╝       ╚═════╝╚══════╝╚═╝
    Mad Penguin Consulting Ltd (c) 2023 (MIT License)

[OrbitCLI] Adding component orbit-component-buttonlegal to my-demo-project
[OrbitCLI] Installing JS component orbit-component-buttonlegal >>>>
...
[OrbitCLI] <<<<
[OrbitCLI] Installing PY component orbit-component-buttonlegal >>>>
[OrbitCLI] <<<<
[OrbitCLI] Adding component orbit-component-zerodocs to my-demo-project
[OrbitCLI] Installing JS component orbit-component-zerodocs >>>>
...
[OrbitCLI] <<<<
[OrbitCLI] Installing PY component orbit-component-zerodocs >>>>
...
[OrbitCLI] <<<<
[OrbitCLI] Running update_components for project my-demo-project
[OrbitCLI] Installing fonts.txt in client/src/components/fonts.template
[OrbitCLI] Installing main.js in client/src
[OrbitCLI] Complete.
```