# Examples of the Shell Component

We covered the database shell above, this shell can be used via a web connection in for the form of a component called "orbit-component-dbshell". This is a very simple component that leverages a build in capability that allow any command line application to be presented in a pane and run remotely within a browser window. So, to add it in;

```python
$ orbit_cli --add my-demo-project --components orbit-component-dbshell

 ██████╗ ██████╗ ██████╗ ██╗████████╗    ██████╗██╗     ██╗
██╔═══██╗██╔══██╗██╔══██╗██║╚══██╔══╝   ██╔════╝██║     ██║
██║   ██║██████╔╝██████╔╝██║   ██║█████╗██║     ██║     ██║
██║   ██║██╔══██╗██╔══██╗██║   ██║╚════╝██║     ██║     ██║
╚██████╔╝██║  ██║██████╔╝██║   ██║      ╚██████╗███████╗██║
 ╚═════╝ ╚═╝  ╚═╝╚═════╝ ╚═╝   ╚═╝       ╚═════╝╚══════╝╚═╝
    Mad Penguin Consulting Ltd (c) 2023 (MIT License)

[OrbitCLI] Adding component orbit-component-dbshell to my-demo-project
[OrbitCLI] Installing JS component orbit-component-dbshell >>>>
...
[OrbitCLI] <<<<
[OrbitCLI] Installing PY component orbit-component-dbshell >>>>
...
[OrbitCLI] <<<<
[OrbitCLI] Running update_components for project my-demo-project
[OrbitCLI] Installing fonts.txt in client/src/components/fonts.template
[OrbitCLI] Installing main.js in client/src
[OrbitCLI] Complete.
```

One caveat here; if you have any local components added with *npx link*, the process calls "npm" which will kill your links. In this instance, once the above completes, reinstate your links with "npx link" as before, then run "orbit_cli --update" as above. You should then see two new components down the left, one of which is called "DB Shell", which gives you all of the capabilities of the database shell shown above, and a second called "Shell" which provides a shell session onto the server.

<img style="background-color: white; height:400px;margin: 1em;border-radius:4px" src="images/demo6.png" />

In terms of performance, it's better than you might expect (!) given the terminal session is comming in via websockets in real-time. If you install a video player for example which is capable of driving video as ascii-art, the terminal session is able to keep up with a reasonably sized full-colour presentation of "Big Buck Bunny" (which is my stock test video).

```bash
$ mpv bbb.mp4 -vo caca
 (+) Video --vid=1 (*) (h264 640x360 24.000fps)
 (+) Audio --aid=1 (*) (aac 2ch 44100Hz)
File tags:
 Comment: license:http://creativecommons.org/licenses/by/3.0/
 Title: Big Buck Bunny - https://archive.org/details/BigBuckBunny_124
```

<img style="background-color: white; height:400px;margin: 1em;border-radius:4px" src="images/demo7.png" />

This all needs documenting in more detail .. watch this space .. 
