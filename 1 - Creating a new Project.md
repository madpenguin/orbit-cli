# Creating a new project

This tool is very opinionated and will install the tool-chain we currently think is the best option for creating and building Orbit Applications and Orbit Components. This may change in the future, but for now working with what *Orbit-CLI* creates is by far the mose efficient approach.

So to start off with, ideally you need a new environment that doesn't already have things installed that might conflict with a new project. At least this should be a clean / empty directory but for demonstration purposes we're going to be using a dedicated user account on a virtual machine. So the first thing you'll do is install Orbit CLI. We're going to assume this is a completely clean container that you don't mind breaking with some sort of Debian or Ubuntu based system installed. (it should work on others, but you might have to translate package names for your package manager)

```python
$ apt install python3-pip git build-essential curl python3.11-venv libsnappy-dev
$ pip3 install orbit_cli --break-system-packages # looks scary, but ...
```

Next we can start the setup;
```python
$ orbit_cli --init application

 ██████╗ ██████╗ ██████╗ ██╗████████╗    ██████╗██╗     ██╗
██╔═══██╗██╔══██╗██╔══██╗██║╚══██╔══╝   ██╔════╝██║     ██║
██║   ██║██████╔╝██████╔╝██║   ██║█████╗██║     ██║     ██║
██║   ██║██╔══██╗██╔══██╗██║   ██║╚════╝██║     ██║     ██║
╚██████╔╝██║  ██║██████╔╝██║   ██║      ╚██████╗███████╗██║
 ╚═════╝ ╚═╝  ╚═╝╚═════╝ ╚═╝   ╚═╝       ╚═════╝╚══════╝╚═╝
    Mad Penguin Consulting Ltd (c) 2023 (MIT License)

[OrbitCLI] Ensuring pyenv is set up ...
[OrbitCLI] Unable to locate pyenv, would you like to install it? [y/n]:
```
So the first thing it's going to want is "pyenv" which is a tool for managing virtual Python environments. This will be useful later with multiple projects and potentially testing against multiple Python versions. It should give you a page full of text, then prompt (note; Orbit CLI will produce coloured text, output from setup tools will be black/white);
```python
[OrbitCLI] Ensuring nvm is set up ...
[OrbitCLI] Unable to locate nvm, would you like to install it? [y/n]:
```
 This is prompting form "nvm" which is a tool for installing and maintaining versions of "node", which is used to compile VUE (and other) components into Javascript, which can then be loaded into the browser.
 ```python
[OrbitCLI] Ensuring npm is set up ...
[OrbitCLI] Unable to locate npm, would you like to install it? [y/n]: 
 ```
 Next we have "npm" which is the node package manager. This will be used to manage external dependencies for the client side of the project. It will also add a default (recent) version of "node" itself.
 ```python
[OrbitCLI] Ensuring poetry is set up ...
[OrbitCLI] Unable to locate poetry, would you like to install it? [y/n]:
 ```
Finally we have "poetry" which is one of the many package managers for Python. Of all the different managers I've come across, to date this one seems to be, overall "the best". It still used "pip", however it has it's own dependency and version checking with work nicely with "pyenv".

Next we come to the actual project setup, thus far we've been setting up a development environment that can be used to run any number of projects (i.e. it's a one-off). This stage is to set up a specific project.
```python
[OrbitCLI] Collecting project information...
[OrbitCLI] Enter your project name (my-demo-project): 
[OrbitCLI] Enter your project description (Just an example Orbit application): 
[OrbitCLI] Enter your name (project author) (John Doe): 
[OrbitCLI] Enter your email address (bugs email) (support@madpenguin.uk): 
[OrbitCLI] Enter your project home page (https://linux.uk): 
[OrbitCLI] Create project my-demo-project? [y/n]: 
```
You should obviously put in values to suit, although they can be changed later, they are used to fill-out multiple templates which sometimes are inter-dependent. That said, if you develop your code as a module (which you really should) then the application itself can be recreated at any time with relatively little effort.
```python
[OrbitCLI] Creating folders ...
[OrbitCLI] Creating folder: apt/etc/systemd/system
[OrbitCLI] Creating folder: apt/usr/local/bin
[OrbitCLI] Creating folder: apt/DEBIAN
[OrbitCLI] Creating folder: apt/opt/my-demo-project/scripts
[OrbitCLI] Creating folder: apt/opt/my-demo-project/web/assets
[OrbitCLI] Creating folder: client/public
[OrbitCLI] Creating folder: client/src/assets
[OrbitCLI] Creating folder: client/src/components
[OrbitCLI] Creating folder: client/src/views
[OrbitCLI] Creating folder: scripts
[OrbitCLI] Creating folder: server/src
[OrbitCLI] Creating folder: server/scripts
[OrbitCLI] Installing README.md in .
[OrbitCLI] Installing Makefile in .
[OrbitCLI] Installing .gitignore in .
[OrbitCLI] Installing .version in .
[OrbitCLI] Installing README.md in server
[OrbitCLI] Installing pyproject.toml in server
[OrbitCLI] Installing orbit.spec in server
[OrbitCLI] Installing Makefile in server
[OrbitCLI] Installing __main__.py in server/src
[OrbitCLI] Installing version.py in server/src
[OrbitCLI] Installing make_keys.sh in server/scripts
[OrbitCLI] Installing control in apt/DEBIAN
[OrbitCLI] Installing postinst in apt/DEBIAN
[OrbitCLI] Installing project.service in apt/etc/systemd/system/my-demo-project.service
[OrbitCLI] Installing make_keys.sh in apt/opt/my-demo-project/scripts
[OrbitCLI] Installing roll_version.py in scripts
[OrbitCLI] Installing index.html in client
[OrbitCLI] Installing Makefile in client
[OrbitCLI] Installing package.json in client
[OrbitCLI] Installing vite.config.js in client
[OrbitCLI] Installing App.vue in client/src
[OrbitCLI] Installing main.js in client/src
[OrbitCLI] Installing main.css in client/src/assets
[OrbitCLI] Installing orbit-logo.png in client/src/assets
[OrbitCLI] Installing favicon.ico in client/public
[OrbitCLI] Installing JS components >>>>
...
[OrbitCLI] <<<<
[OrbitCLI] Installing PY components >>>>
...
[OrbitCLI] <<<<
[OrbitCLI] Running update_components for project my-demo-project
[OrbitCLI] Installing fonts.txt in client/src/components/fonts.template
[OrbitCLI] Installing main.js in client/src
[OrbitCLI] Complete.
```
At this point you should have something that runs, but doesn't do anything that's really visible.