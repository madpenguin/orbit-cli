.PHONEY: all version publish_local_server publish_global_server publish_local publish_global

all:
	@echo "local                 - set local registry"
	@echo "global                - set global registry"
	@echo "version               - roll the version number"
	@echo "publish_local_server  - publish the server component locally"
	@echo "publish_global_server - publish the server component globally"
	@echo "publish_local         - publish all to local repos"
	@echo "publish_global        - publish all to global repos"

setup:
	@cd server && poetry source add borg http://borg:8080 --priority="primary"
	@cd server && poetry poetry source add pypi --priority="supplemental"

version:
	@./scripts/roll_version.py

publish_local_server:
	@echo "publish :: server :: local"
	@cd server && poetry publish --build --repository borg

publish_global_server:
	@echo "publish :: server :: global"
	@cd server && poetry publish --build 

publish_local: version publish_local_server

publish_global: version publish_global_server
