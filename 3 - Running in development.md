# Running in Development Mode

Now we have something worthwhile, we can spin it up in development mode as follows;
```bash
shell1> cd ~/my-demo-project/client
shell1> npm run dev
..
shell2> cd ~/my-demo-project/server
shell2> pyenv activate my-demo-project
shell2> make run_dev
```
Unless the ports are already in use, by default it should run the Vite server on port 5173 (which provides the javascript) and the Python server on port 8445. To get access you will need to connect to https://localhost:8445. If you have installed (as I have) into a virtual machine, then in a different session do;
```bash
ssh -L 8445:localhost:8445 -L 5173:localhost:5173 (hostname)
```
Which will set up a tunnel so you can access port 8445 on (hostname) by accessing "localhost:8445". Unfortunately browsers don't like self-signed https certificates so if you try to access the virtual machine directly on port 8445, it's likely not to be happy with the certificate. If you are using chrome and it still throws a wobbler at the certificate, take a look in your browser at;
```bash
chrome://flags/#allow-insecure-localhost
```
(and enable it), What you should see is something like this;

<img style="background-color: white; height:400px;margin: 1em;border-radius:4px" src="images/demo1.png" />

If you click on the *Legal* button it should pop up a sample *Terms and Conditions* page which isn't very interesting but is a start. If we now look at configuring the module, strictly speaking this is nothing to do with the Orbit CLI script, but it's nice to get a visible result from the answer.

So if we first give ourselves admin permission, we should be able to access more functionality, to do this;
```bash
cd ~/my-demo-project/server
pyenv activate my-demo-project
$ python -m src --users
┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━┓
┃ Host Id                              ┃ User Id                              ┃ Active ┃
┡━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━┩
│ 8a26e9f2-13ed-48c9-be6c-ab327b579765 │ 0c919afc-e433-49b8-90af-5347d1dfdb3b │ True   │
└──────────────────────────────────────┴──────────────────────────────────────┴────────┘
```
You should be the only user, and it's the "Host Id" we're interested in, so;
```bash
python -m src --add-user-group 8a26e9f2-13ed-48c9-be6c-ab327b579765 admin
```
If you run the *users* command again it should now list you as a member of the *admin* group, you should also see on the web page that two buttons have appeared in the bottom left. (Edit and Refresh) Note that the permissions are held in a table, and your component of that table is mapped to the browser, so the instant your permissions change, anything in the application that is dependent on the permissions, also changes. In this case, the visibility of the *Edit* button.

##### This particular module relies on the NLTK toolkit, which tokenises data for the full test indexing module. This requires (currently) a quick hack to enable properly;
```bash
cd ~/my-demo-project/server
pyenv activate my-demo-project
python3
>>> import nltk
>>> nltk.download('punkt')
>>> nltk.download('stopwords')
>>> exit()
```

For demonstration purposes, if you click on "Edit", then "Add" and fill out the form with a project name of "orbit-database", branch "main" and give it a name of "Orbit Database", then "Add".

<img style="background-color: white; height:400px;margin: 1em;border-radius:4px" src="images/add.png" />

You should then be able to open up the file tree, pick a bunch of items (say the numbered .md files, README etc) then *Done*, you should get something like this, which is *more* interesting ...

<img style="background-color: white; height:400px;margin: 1em;border-radius:4px" src="images/demo2.png" />